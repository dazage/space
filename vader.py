#                   For the brave souls who get this far: You are the chosen ones,                      #
#                   the valiant knights of programming who toil away, without rest,                     #
#                   fixing our most awful code. To you, true saviors, kings of men,                     #
#                   I say this: PLIES GIEV 10/10 EINKUNN KTHX                                           #
import pygame
import random
import math
import time

# If we want to use sprites we create a class that inherits from the Sprite class.
# Each class has an associated image and a rectangle.


class Alien(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = alien_image
        self.rect = self.image.get_rect()


class Asteroid(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = asteroid_image
        self.rect = self.image.get_rect()


class Boss(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = boss_image
        self.rect = self.image.get_rect()


class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = player_image
        self.rect = self.image.get_rect()

    def collide(self, sprites):
        for sprite in sprites:
            if pygame.sprite.collide_rect(self, sprite):
                return sprite


class Missile(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = missile_image
        self.rect = self.image.get_rect()


def calculate_pos(direction, x, width):
    if (x * direction) < 0:
        return(width * direction) + x
    elif x == width or x == 0:
        return(-width) * direction
    else:
        return (width - (x * direction)) * - direction


# Initializes pygame
pygame.init()

# DISPLAY #
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
pygame.display.set_caption("I said pass the juice, not ga--")

# GRAPHICS #
alien_image = pygame.image.load('sprites/green_ball.png')
asteroid_image = pygame.image.load('sprites/asteroid.png')
back_1 = pygame.image.load('sprites/nebula_blue.png')
back_2 = pygame.image.load('sprites/nebula_blue.png')
boss_image = pygame.image.load('sprites/boss.png')
missile_image = pygame.image.load('sprites/missile.png')
player_image = pygame.image.load('sprites/spaceship.png')

# AUDIO #
pygame.mixer.init()
pygame.mixer.music.load('music/Shiroyama.ogg')
pygame.mixer.music.play()

# This is a list of 'sprites.' Each block in the program is
# added to this list. The list is managed by a class called 'Group.'
asteroid_list = pygame.sprite.Group()
alien_list = pygame.sprite.Group()
boss_list = pygame.sprite.Group()
# Group to hold missiles
missile_list = pygame.sprite.Group()
# This is a list of every sprite. All blocks and the player block as well.
# The god damn robots are stealing our jobs.
# Having an extra group for all sprites makes it far easier to draw them
# all onto the screen.  In fact it's done by a single line of code(line 122)
all_sprites_list = pygame.sprite.Group()

# Create the x axis variable for the background to scroll
x1 = 0
n = 1

# Create a range of asteroids continuously
for i in range(random.randrange(3, 10)):
    asteroid = Asteroid()
    asteroid.rect.x = random.randrange(SCREEN_WIDTH - 20)
    asteroid.rect.y = -random.randrange(120)

    asteroid_list.add(asteroid)
    all_sprites_list.add(asteroid)

# Create a range of aliens continuously
for i in range(random.randrange(3, 10)):
    alien = Alien()
    alien.rect.x = random.randrange(SCREEN_WIDTH - 20)
    alien.rect.y = -random.randrange(120)

    alien_list.add(alien)
    all_sprites_list.add(alien)

# Create a player block
player = Player()
player.rect.x = 320
player.rect.y = 340
player_speed = 7

# Adds player to all sprites list
all_sprites_list.add(player)

# Enemies
alien = 0
boss = 0
boss_spawner = 0
px = 0
py = 0
# Scores
kills = 0
kills_needed = 10
score = 0
win = 0
# Status effects
shield = False

# Loop until the user clicks the close button.
done = False
game_over = False
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
RUNNING = 0
PAUSE = 1
state = 0
SPAWNEVENT = pygame.USEREVENT + 1
pygame.time.set_timer(SPAWNEVENT, 3000)

# -------- Main Program Loop -----------
# We need to check out what happens when the player hits the space bar in order to "shoot" the jews.
# A new missile is created and gets it's initial position in the "middle" of the player.
# Then this missile is added to the missile sprite-group and also to the all_sprites group.
while not done:
    for event in pygame.event.get():
        if event.type == SPAWNEVENT:
            for i in range(random.randrange(3, 10)):
                alien = Alien()
                alien.rect.x = random.randrange(SCREEN_WIDTH - 20)
                alien.rect.y = -random.randrange(120)

                alien_list.add(alien)
                all_sprites_list.add(alien)
            for i in range(random.randrange(3, 10)):
                asteroid = Asteroid()
                asteroid.rect.x = random.randrange(SCREEN_WIDTH - 20)
                asteroid.rect.y = -random.randrange(120)

                asteroid_list.add(asteroid)
                all_sprites_list.add(asteroid)
            boss_spawner += 1
            boss = 0  # This was added because otherwise you wouldn't be able to complete the game, since the boss doesn't work

        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if boss == 1:
                    print("It's no use!")
                else:                                         #At a certain point commenting becomes obsolete.
                    shot = Missile()
                    shot.rect.x = player.rect.x + 10
                    shot.rect.y = player.rect.y - 10
                    missile_list.add(shot)
                    all_sprites_list.add(shot)
            if event.key == pygame.K_p:
                state = PAUSE
            if event.key == pygame.K_s:
                state = RUNNING
            if event.key == pygame.K_q:
                done = True

    if state == RUNNING:
        if boss_spawner == 2:
            boss_spawner += 1
            boss = 1
            bosss = Boss()
            alien.rect.x = random.randrange(SCREEN_WIDTH - 20)
            alien.rect.y = -random.randrange(120)

            boss_list.add(bosss)
            all_sprites_list.add(bosss)

        key = pygame.key.get_pressed()
        if key[pygame.K_LEFT]:
            if player.rect.x < 0:
                if boss == 1:
                    player.rect.x -= 0
                else:
                    player.rect.x = 800
            player.rect.x -= player_speed
        elif key[pygame.K_RIGHT]:
            if player.rect.x > 800:
                if boss == 1:
                    player.rect.x -= 0
                else:
                    player.rect.x = -5
            player.rect.x += player_speed
        elif key[pygame.K_UP]:
            if player.rect.y < -5:
                if boss == 1:
                    player.rect.y -= 0
                else:
                    player.rect.y = 800
            player.rect.y -= player_speed
        elif key[pygame.K_DOWN]:
            if player.rect.y > 800:
                if boss == 1:
                    player.rect.y -= 0
                else:
                    player.rect.y = -5
            player.rect.y += player_speed

        if kills >= 10:
            shield = True

        if player.collide(alien_list):  # This appears to be extremely buggy. Sometimes it takes 10+, sometimes just 1
            if not shield:
                done = True
            else:
                print("Ha ha! You have a shield! Nothing can stop you!")
        if player.collide(asteroid_list):  # This appears to be extremely buggy. Sometimes it takes 10+, sometimes just 1
            if not shield:
                done = True
            else:
                print("Ha ha! You have a shield! Nothing can stop you!")

        if score >= 35:  # Ha ha this was == 20 for a bit and I didn't realise why I didn't win after adding aliens!
            win += 1
            done = True


        # Below is another good example of Sprite and SpriteGroup functionality.
        # It is now enough to see if some missile has collided with some asteroid
        # and if so, they are removed from their respective groups.
        # In other words:  A missile exploded and so did an asteroid.

        # See if the player block has collided with anything.
        missile_hit_aliens_list = pygame.sprite.groupcollide(missile_list, alien_list, True, True)
        missile_hit_asteroids_list = pygame.sprite.groupcollide(missile_list, asteroid_list, True, True)
        blocks_hit_list = pygame.sprite.spritecollide(player, asteroid_list, True)

        for block in missile_hit_aliens_list:
            score += 3
            kills += 1
            kills_needed -= 1

        for block in missile_hit_asteroids_list:
            score += 1
            kills += 1
            kills_needed -= 1

        # Missiles move at a constant speed up the screen, towards the enemy
        for shot in missile_list:
            shot.rect.y -= 5

        # Set speed at which the asteroids move down
        if score >= 0:
            min_asteroid_speed = 1
            max_asteroid_speed = 2
        if score >= 3:
            min_asteroid_speed = 2
            max_asteroid_speed = 3
        if score >= 7:
            min_asteroid_speed = 3
            max_asteroid_speed = 4
        if score >= 15:
            min_asteroid_speed = 4
            max_asteroid_speed = 5
        # Set speed at which the aliens move down
        if score >= 0:
            min_alien_speed = 1
            max_alien_speed = 2
        if score >= 3:
            min_alien_speed = 2
            max_alien_speed = 4
        if score >= 7:
            min_alien_speed = 4
            max_alien_speed = 6
        if score >= 15:
            min_alien_speed = 6
            max_alien_speed = 8

        asteroid_speed = random.randrange(min_asteroid_speed, max_asteroid_speed)
        alien_speed = random.randrange(min_alien_speed, max_alien_speed)
        boss_speed = 4

        for block in asteroid_list:
            block.rect.y += asteroid_speed

        for block in alien_list:
            block.rect.y += alien_speed

        for block in boss_list:
            dx = block.rect.x - player.rect.x   #       At least I tried        #
            dy = block.rect.y - player.rect.y   #       At least I tried        #
            dist = math.hypot(dx, dy)           #       The boss does spawn     #
            dx = dx / dist                      #       But he goes in the      #
            dy = dy / dist                      #       wrong direction. :-(    #
            block.rect.x += dx * boss_speed     #       At least I tried        #
            block.rect.y += dy * boss_speed     #       At least I tried        #

        # Background scrolls
        x_pos = calculate_pos(n, x1, SCREEN_WIDTH)

        screen.blit(back_1, (x1, 0))
        screen.blit(back_2, (x_pos, 0))

        x1 += n
        if x1 == SCREEN_WIDTH or x1 == -SCREEN_WIDTH:
            x1 = 0

        # Draw all the spites
        all_sprites_list.draw(screen)

        # Create font to show score
        font = pygame.font.SysFont('monospace', 30, True, False)
        pause_text = font.render("PAUSED", True, [255, 255, 255])
        score_text = font.render("Score: " + str(score) + "/40", True, [0, 255, 0])
        screen.blit(score_text, [25, 25])
        kills_text = font.render("Kills: " + str(kills), True, [255, 255, 0])
        screen.blit(kills_text, [25, 535])
        if kills_needed > 0:
            kills_to_shield = font.render("Kills needed for shield: " + str(kills_needed), True, [255, 0, 255])
            screen.blit(kills_to_shield, [315, 535])
        else:
            kills_to_shield = font.render("Kills needed for shield: 0", True, [255, 0, 255])
            screen.blit(kills_to_shield, [325, 535])
        if kills >= 10:
            shield_text = font.render("Shield engaged ", True, [0, 255, 255])
            screen.blit(shield_text, [525, 25])

    elif state == PAUSE:
        screen.blit(pause_text, [350, 250])


    # Go ahead and update the screen with what we've drawn.
    # This MUST happen after all the other drawing commands.
    pygame.display.flip()

    # Limit to 60 frames per second
    clock.tick(60)
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

screen.fill(BLACK)
game_over_font = pygame.font.SysFont('monospace', 18, True, False)
if win >= 1:
    end_text = font.render("You win! Thanks for playing!", True, [255, 255, 255])
    screen.blit(end_text, [150, 250])
    pygame.display.update()
    time.sleep(3)
else:
    end_text = game_over_font.render("The oppression of the Sith will never return. You have lost.", True, WHITE)
    screen.blit(end_text, [75, 250])
    pygame.display.update()
    time.sleep(3)
pygame.quit()
